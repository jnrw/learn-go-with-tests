package maps

import (
	"testing"
)

const (
	word       = "Word"
	definition = "Definition"
)

func TestSearch(t *testing.T) {
	dictionary := Dictionary{word: definition}

	t.Run("known key", func(t *testing.T) {
		got, _ := dictionary.Search(word)
		want := definition
		assertStrings(t, got, want)
	})

	t.Run("unknown key", func(t *testing.T) {
		_, err := dictionary.Search("unknown")
		assertError(t, err, ErrNotFound)
	})
}

func TestAdd(t *testing.T) {
	dictionary := Dictionary{}

	t.Run("new word", func(t *testing.T) {
		dictionary.Add(word, definition)
		got, err := dictionary.Search(word)
		assertNotError(t, err, "should find added word")
		assertStrings(t, got, definition)
	})

	t.Run("existing word", func(t *testing.T) {
		err := dictionary.Add(word, definition)
		assertError(t, err, ErrWordExists)
		got, _ := dictionary.Search(word)
		assertStrings(t, got, definition)
	})
}

func TestUpdate(t *testing.T) {
	dictionary := Dictionary{word: definition}
	const (
		newDefinition = "New definition"
		unknownWord   = "Unknown"
	)

	t.Run("existing word", func(t *testing.T) {
		err := dictionary.Update(word, newDefinition)
		assertNotError(t, err, "should update word")
	})

	t.Run("unknown word", func(t *testing.T) {
		err := dictionary.Update(unknownWord, definition)
		assertError(t, err, ErrWordDoesNotExists)
	})

	t.Run("blank word", func(t *testing.T) {
		err := dictionary.Update("", definition)
		assertError(t, err, ErrWordDoesNotExists)
	})
}

func TestDelete(t *testing.T) {
	dictionary := Dictionary{word: definition}
	const unknownWord = "Unknown"

	t.Run("existing word", func(t *testing.T) {
		dictionary.Delete(word)
		_, err := dictionary.Search(word)
		assertError(t, err, ErrNotFound)
	})

	t.Run("unknown word", func(t *testing.T) {
		dictionary.Delete(unknownWord)
		_, err := dictionary.Search(word)
		assertError(t, err, ErrNotFound)
	})
}

func assertNotError(t *testing.T, err error, message string) {
	t.Helper()
	if err != nil {
		t.Error(message, err)
	}
}

func assertError(t *testing.T, got, want error) {
	t.Helper()
	if got == nil {
		t.Fatal("expected to get an error.")
	}

	if got != want {
		t.Errorf("got error %q want %q", got, want)
	}
}

func assertStrings(t *testing.T, got, want string) {
	t.Helper()
	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}
