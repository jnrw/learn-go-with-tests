package bank

import "errors"

var ErrInsuficcientFunds = errors.New("Cannot withdraw, insufficient funds")

type Wallet struct {
	balance Bitcoin
}

func (w *Wallet) Deposit(amount Bitcoin) { // w is a reference of wallet passed as argument
	w.balance += amount
}

func (w Wallet) Balance() Bitcoin { // w is a copy of wallet passed as argument
	return w.balance
}

func (w *Wallet) Withdraw(amount Bitcoin) error {
	if amount > w.Balance() {
		return ErrInsuficcientFunds
	}
	w.balance -= amount
	return nil
}
