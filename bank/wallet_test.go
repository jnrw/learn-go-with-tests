package bank

import (
	"testing"
)

func TestWallet(t *testing.T) {
	t.Run("Deposit", func(t *testing.T) {
		amount := Bitcoin(10)
		wallet := Wallet{}
		wallet.Deposit(amount)
		assertCorrectBalance(t, amount, wallet.Balance())
	})

	t.Run("Withdraw", func(t *testing.T) {
		amount := Bitcoin(10)
		wallet := Wallet{Bitcoin(10)}
		err := wallet.Withdraw(amount)
		assertNoError(t, err)
		assertCorrectBalance(t, Bitcoin(0), wallet.Balance())
	})

	t.Run("Withdraw insufficient founds", func(t *testing.T) {
		startBalance := Bitcoin(10)
		amount := Bitcoin(20)
		wallet := Wallet{startBalance}
		err := wallet.Withdraw(amount)
		assertCorrectBalance(t, startBalance, wallet.Balance())
		assertError(t, err, ErrInsuficcientFunds)
	})
}

func assertCorrectBalance(t *testing.T, expected, result Bitcoin) {
	t.Helper()
	if result != expected {
		t.Errorf("Expected %s as wallet balance, but got %s.", expected, result)
	}
}

func assertNoError(t *testing.T, got error) {
	t.Helper()
	if got != nil {
		t.Fatalf("got %q error, but did not want one", got)
	}
}

func assertError(t *testing.T, got error, want error) {
	t.Helper()
	if got == nil {
		t.Fatal("didn't get an error but wanted one")
	}

	if got != want {
		t.Errorf("got %q, want %q", got, want)
	}
}
