package slices

// Sum an array of numbers
func Sum(values []int) (sum int) {
	for _, value := range values {
		sum += value
	}
	return
}

// SumAll slices separately
func SumAll(slices ...[]int) (sums []int) {
	for _, slice := range slices {
		sums = append(sums, Sum(slice))
	}
	return
}

// SumAllTails from slices separately
func SumAllTails(slices ...[]int) (sums []int) {
	for _, slice := range slices {
		if len(slice) == 0 {
			sums = append(sums, 0)
		} else {
			sums = append(sums, Sum(slice[1:]))
		}
	}
	return
}
