package slices

import (
	"reflect"
	"testing"
)

func TestSum(t *testing.T) {
	assertCorrectMessage := func(t *testing.T, expected, result int, values []int) {
		t.Helper()
		if result != expected {
			t.Errorf("Expected %d given %v, but got %d", expected, values, result)
		}
	}

	t.Run("Calculating sum of {1, 2, 3}", func(t *testing.T) {
		values := []int{1, 2, 3}
		sum := Sum(values)
		expected := 6
		assertCorrectMessage(t, expected, sum, values)
	})

	t.Run("Calculating sum of {1, 2, 3, 4, 5}", func(t *testing.T) {
		values := []int{1, 2, 3, 4, 5}
		sum := Sum(values)
		expected := 15
		assertCorrectMessage(t, expected, sum, values)
	})

	t.Run("Calculating sum of an empty slice", func(t *testing.T) {
		values := []int{}
		sum := Sum(values)
		expected := 0
		assertCorrectMessage(t, expected, sum, values)
	})
}

func TestSumEachSlice(t *testing.T) {
	assertSums := func(t *testing.T, expected, result []int) {
		t.Helper()
		if !reflect.DeepEqual(expected, result) {
			t.Errorf("expected %d but got %v", expected, result)
		}
	}

	t.Run("Safely sum of empty slices", func(t *testing.T) {
		expected := []int{0, 0}
		result := SumAllTails([]int{}, []int{})
		assertSums(t, expected, result)
	})

	t.Run("Sum of filled slices", func(t *testing.T) {
		expected := []int{0, 6, 15}
		result := SumAll([]int{}, []int{0, 1, 2, 3}, []int{0, 1, 2, 3, 4, 5})
		assertSums(t, expected, result)
	})

}

func TestSumAllTails(t *testing.T) {
	assertSums := func(t *testing.T, expected, result []int) {
		t.Helper()
		if !reflect.DeepEqual(expected, result) {
			t.Errorf("expected %d but got %v", expected, result)
		}
	}

	t.Run("Safely sum of tails from empty slices", func(t *testing.T) {
		expected := []int{0, 0}
		result := SumAllTails([]int{}, []int{1})
		assertSums(t, expected, result)
	})

	t.Run("Make the sum of tails from filled slices", func(t *testing.T) {
		expected := []int{5, 14}
		result := SumAllTails([]int{1, 2, 3}, []int{1, 2, 3, 4, 5})
		assertSums(t, expected, result)
	})

}
