package iteration

import (
	"fmt"
	"testing"
)

func TestRepeat(t *testing.T) {
	expected := "aaaaa"
	sequence := Repeat("a", 5)

	if sequence != expected {
		t.Errorf("expected %q but got %q", expected, sequence)
	}
}

func ExampleRepeat() {
	sequence := Repeat("a", 5)
	fmt.Println(sequence)
	// Output: aaaaa
}

func BenchmarkRepeat(b *testing.B) {
	var sequence string
	for i := 0; i < b.N; i++ {
		sequence = Repeat("a", 5)
	}
	if sequence != "" {
	}
}

func TestStdRepeat(t *testing.T) {
	expected := "aaaaa"
	sequence := StdRepeat("a", 5)

	if sequence != expected {
		t.Errorf("expected %q but got %q", expected, sequence)
	}
}

func ExampleStdRepeat() {
	sequence := StdRepeat("a", 5)
	fmt.Println(sequence)
	// Output: aaaaa
}

func BenchmarkStdRepeat(b *testing.B) {
	var sequence string
	for i := 0; i < b.N; i++ {
		sequence = StdRepeat("a", 5)
	}
	if sequence != "" {
	}
}
