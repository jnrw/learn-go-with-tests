package iteration

import "strings"

func Repeat(str string, times int) (sequence string) {
	for i := 0; i < 5; i++ {
		sequence += str
	}
	return
}

func StdRepeat(str string, times int) (sequence string) {
	sequence = strings.Repeat(str, times)
	return
}
