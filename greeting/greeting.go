package greeting

import (
	"strings"
)

const (
	spanish            = "Spanish"
	french             = "French"
	englishHelloPrefix = "Hello, "
	spanishHelloPrefix = "Hola, "
	frenchHelloPrefix  = "Bonjour, "
	helloSufix         = "!"
	defaultName        = "World"
)

func Hello(name, language string) string {
	if strings.TrimSpace(name) == "" {
		name = defaultName
	}

	return greetingPrefix(language) + name + helloSufix
}

func greetingPrefix(language string) (prefix string) {
	switch language {
	case spanish:
		prefix = spanishHelloPrefix
	case french:
		prefix = frenchHelloPrefix
	default:
		prefix = englishHelloPrefix
	}
	return
}
