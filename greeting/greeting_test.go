package greeting

import "testing"

func TestHello(t *testing.T) {
	assertCorrectMessage := func(t *testing.T, want, got string) {
		t.Helper()
		if got != want {
			t.Errorf("got %q want %q", got, want)
		}
	}
	t.Run("saying hello to people", func(t *testing.T) {
		want := "Hello, Gopher!"
		got := Hello("Gopher", "")
		assertCorrectMessage(t, want, got)
	})

	t.Run("saying \"Hello, World!\" when an empty string is supplied", func(t *testing.T) {
		want := "Hello, World!"
		got := Hello("", "")
		assertCorrectMessage(t, want, got)
	})

	t.Run("saying \"Hello, World!\" when an string contains only whitespace character", func(t *testing.T) {
		want := "Hello, World!"
		got := Hello(" ", "")
		assertCorrectMessage(t, want, got)
	})

	t.Run("saying \"Hello, World!\" to people in Spanish", func(t *testing.T) {
		want := "Hola, Gopher!"
		got := Hello("Gopher", "Spanish")
		assertCorrectMessage(t, want, got)
	})

	t.Run("saying \"Hello, World!\" to people in French", func(t *testing.T) {
		want := "Bonjour, Gopher!"
		got := Hello("Gopher", "French")
		assertCorrectMessage(t, want, got)
	})
}
