package types

import "testing"

func TestAreaByTableDrivenTests(t *testing.T) {
	areaTests := []struct {
		name  string
		shape Shape
		want  float64
	}{
		{name: "Area rectangle", shape: Rectangle{12, 6}, want: 72.0},
		{name: "Area circle", shape: Circle{10}, want: 314.1592653589793},
		{name: "Area triangle", shape: Triangle{12, 6}, want: 36.0},
	}

	for _, tt := range areaTests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.shape.Area()
			if got != tt.want {
				t.Errorf("%s %#v got %g want %g", tt.name, tt.shape, got, tt.want)
			}
		})

	}
}
