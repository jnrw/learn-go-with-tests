package types

import "testing"

func TestPerimeter(t *testing.T) {
	assertPerimeter := func(t *testing.T, shape Shape, expected float64) {
		t.Helper()
		result := shape.Perimeter()
		if expected != result {
			t.Errorf("Expected perimeter for %#v is %g but got %g", shape, expected, result)
		}
	}

	t.Run("Perimeter of Circle", func(t *testing.T) {
		circle := Circle{10}
		assertPerimeter(t, circle, 62.83185307179586)
	})

	t.Run("Perimeter of Rectangle", func(t *testing.T) {
		rectangle := Rectangle{10, 5}
		assertPerimeter(t, rectangle, 30.0)
	})
}

func TestArea(t *testing.T) {
	assertArea := func(t *testing.T, shape Shape, expected float64) {
		t.Helper()
		result := shape.Area()
		if expected != result {
			t.Errorf("Expected area for %#v is %g but got %g", shape, expected, result)
		}
	}

	t.Run("Area of Circle", func(t *testing.T) {
		circle := Circle{10}
		assertArea(t, circle, 314.1592653589793)
	})

	t.Run("Area of Rectangle", func(t *testing.T) {
		rectangle := Rectangle{10, 5}
		assertArea(t, rectangle, 50.0)
	})
}
